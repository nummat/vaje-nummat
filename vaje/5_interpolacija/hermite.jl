""" 
    h = HermitovPolinom(interval, vrednosti, odvodi)

Interpolant za Hermitovo interpolacijo na intervalu `interval = (a, b)` in vrednosti funkcije `vrednosti`
in vrdnosti ovodov `odvodi`. Hermitov polinom interpolira podatke `f(a)`, `f(b)`, `f'(a)` in `f'(b)`.
"""
struct HermitovPolinom
    interval
    f
    df
end

p00(t) = t^2*(2t - 3) + 1 # f(0) = 1
p10(t) = t^2*(-2t + 3) # f(1) = 1
p01(t) = t*(t - 1)^2 # f'(0) = 1
p11(t) = t^2*(t-1) # f'(1) = 1

"""
    y = vrednost(h::HermitovPolinom, x)

Izračunaj vrednost hermitovega polinoma `h` v točki `x`.
"""
function vrednost(h::HermitovPolinom, x)
    a, b = h.interval
    t = (x - a) / (b - a)
    fa, fb = h.f
    dfa, dfb = h.df
    fa * p00(t) + (b - a)*dfa * p01(t) + fb * p10(t) + (b - a)*dfb * p11(t) 
end

# Primer

h = HermitovPolinom((0, pi/2), (1, 0), (0, -1))
a, b = h.interval
cos(0.5) - vrednost(h, 0.5)

using Plots

scatter([h.interval[1], h.interval[2]], [h.f[1], h.f[2]])
plot!(cos, 0, pi/2)
plot!(x -> vrednost(h, x), 0, pi/2)

"""
    hz = HermitovZlepek(x, f, df)

Zgradi hermitov zlepek za vrednosti x v tabeli `x`, 
vrednosti funkcije v tabeli `f` in vrednosti odvodov v tabeli `df`.
"""
struct HermitovZlepek
    x
    f
    df
end

import Base.length

length(h::HermitovZlepek) = length(h.x)

"""
    y = vrednost(h::HermitovZlepek, x)

Izračunaj vrednost Hermitovega zlepka `h` v dani točki `x`.
"""
function vrednost(h::HermitovZlepek, x)
    # poiščemo interval [xi, x(i+1)] na katerem leži x
    # optimalno je iskanje z bisekcijo oziroma s formulo za ekvidistančne točke
    n = length(h)
    for i=1:(n-1)
        if h.x[i] <= x <= h.x[i+1]
            hermitov_polinom = HermitovPolinom(h.x[i:i+1], h.f[i:i+1], h.df[i:i+1])
            # izračunamo vrednost zlepka
            return vrednost(hermitov_polinom, x)
        end
    end
    throw("Vrednost ni znotraj definicijskeg območja zlepka")
end

# Primer 

x = LinRange(0, pi, 5)
f = cos.(x)
df = -sin.(x)

zlepek = HermitovZlepek(x, f, df)

vrednost(zlepek, 1)
vrednost(zlepek, -5)

scatter(x, f)
plot!(cos, 0, pi)
plot!(x -> vrednost(zlepek, x), 0, pi)

plot(x -> cos(x) - vrednost(zlepek, x), 0, pi)

# Ocena za napako |p - f| < f^(4)(ξ)/24*((b-a)/2)^4 = f^(4)(ξ)/384*(b-a)^4
# https://en.wikipedia.org/wiki/Polynomial_interpolation#Interpolation_error
h = x[2] - x[1]
err = 1/384*h^4

# Numerična ocena za napako

t = LinRange(0, 1, 100)

maximum(cos.(t) -[vrednost(zlepek, x) for x in t]) 