"""
    utezi, vozli = simpson(a, b, n)

Izračunaj uteži in vozle za sestavljeno Simpsonovo pravilo za integral na intervalu [a, b]
razdeljen na `n` enakih delov.

## Primer

```julia
# izračunaj integral sin na [0, pi] s Simpsonovo kvadraturo na 5 podintervalih
w, x = simpson(0, pi, 5)
I = dot(w, sin.(x))  # utežena vsota funkcijskih vrednosti
napaka = I - (cos(pi) - cos(0))
```
"""
function simpson(a, b, n)
    h = (b - a)/n
    vozli = LinRange(a, b, 2n + 1)
    utezi = h/6 * vcat([1], repeat([4, 2], n - 1), [4, 1])
    return utezi, vozli
end

# Primer:
# Izračunaj integral sin na [0, pi] s Simpsonovo kvadraturo na 5 podintervalih.
using LinearAlgebra
w, x = simpson(0, pi, 5)
I = dot(w, sin.(x))  # utežena vsota funkcijskih vrednosti
napaka = I - (-cos(pi) + cos(0))

"""
    I = integral_simpson(fun, a, b, n)

Izračuanj integral funkcije `fun` na intervalu `[a, b]` s sestavljeno Simpsonovo formulo
na `n` podintervalih.
"""
function integral_simpson(fun, a, b, n)
    w, x = simpson(a, b, n)
    return dot(w, fun.(x))
end

"""
    integrator = simpson_integrator(a, b, n)

Vrni funkcijo, ki integrira dano funkcijo na intevalu [a, b] s sestavljenim Simpsonovim pravilom.
"""
function simpson_integrator(a, b, n)
    w, x = simpson(a, b, n)
    return (fun) -> dot(w, fun.(x))
end

"""
    utezi, vozlisca = sredinska(a, b, n)

Izračunaj uteži in vozlišča za sestavljeno sredninsko formulo za računanje integrala.
"""
function sredinska(a, b, n)
    h = (b - a)/n
    utezi = h * ones(n)
    vozli = LinRange(a+h/2, b-h/2, n)
    return utezi, vozli
end

"""
    I = integral_sredinska(fun, a, b, n)

Izračuanj integral funkcije `fun` na intervalu `[a, b]` s sestavljeno sredinsko formulo
na `n` podintervalih.
"""
function integral_sredinska(fun, a, b, n)
    w, x = sredinska(a, b, n)
    return dot(w, fun.(x))
end

# Primerjava Simpsonove in sredinske formule za integral funckije sin na [0, pi]

I_pravi = 2 # (-cos(pi) + cos(pi))
n_ji = [2^i for i=1:10]
napaka_simpson = [integral_simpson(sin, 0, pi, n) - I_pravi for n in n_ji]
napaka_sredinska = [integral_sredinska(sin, 0, pi, n) - I_pravi for n in n_ji]

using Plots

scatter(log10.(n_ji), log10.(abs.(napaka_simpson)))
scatter!(log10.(n_ji), log10.(abs.(napaka_sredinska)))